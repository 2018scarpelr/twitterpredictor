from tweet_collection.to_dataframe import transform_to_dataframe
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from tweet_collection.twitter_connection_setup import twitter_setup

connexion = twitter_setup()
tweets =connexion.user_timeline(id="raffaellafico", count=2000)
data=transform_to_dataframe(tweets)


tfol = pd.Series(data=data['followers'].values, index=data['date'])


# Followers per user's tweet visualization:

tfol.plot(figsize=(16,4), label="Followers", legend=True)

plt.show()
