from tweet_collection.to_dataframe import transform_to_dataframe
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from tweet_collection.twitter_connection_setup import twitter_setup

connexion = twitter_setup()
tweets = connexion.search("Emmanuel Macron", language="french", rpp=100)
data=transform_to_dataframe(tweets)


tfav = pd.Series(data=data['likes'].values, index=data['date'])
tret = pd.Series(data=data['RTs'].values, index=data['date'])

# Likes vs retweets visualization:
tfav.plot(figsize=(16,4), label="Likes", legend=True)
tret.plot(figsize=(16,4), label="Retweets", legend=True)

plt.show()
