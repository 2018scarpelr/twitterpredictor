import dash
import dash_core_components as dcc
import dash_html_components as html
from tweet_analysis.analise_pos_neg_candidate import tweet_sentiment
from tweet_collection.collect_candidate_actuality_tweets import *

candidate_number = [1,2]
porcent_neg = []
porcent_neu = []
porcent_pos = []
connexion = twitter_setup()
for candidate in candidate_number:
    queries = get_candidate_queries(candidate,'../CandidateData')
    tweets = get_tweets_from_candidates_search_queries(queries, connexion)
    porcent_neg_i,porcent_neu_i,porcent_pos_i = tweet_sentiment(tweets)
    porcent_pos.append(porcent_pos_i)
    porcent_neu.append(porcent_neu_i)
    porcent_neg.append(porcent_neg_i)

data = []
for i in range(len(porcent_pos)):
    d = {'x':['Positive', 'Neutral', 'Negative'],'y':[porcent_pos[i], porcent_neu[i], porcent_neg[i]], 'type': 'bar', 'name': 'Candidate {}'.format(i+1)}
    data.append(d)
print(data)

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(children=[
    html.H1(children='Hello Dash'),

    html.Div(children='''
        Dash: A web application framework for Python.
    '''),

    dcc.Graph(
        id='example-graph',
        figure={
            'data': data,
            'layout': {
                'title': 'Dash Data Visualization'
            }
        }
    )
])

if __name__ == '__main__':
    app.run_server(debug=False)
