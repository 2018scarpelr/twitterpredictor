from tweet_collection.to_dataframe import transform_to_dataframe
import numpy as np

def tweet_rt_max(data):
    rt_max  = np.max(data['RTs'])
    id_rt_max = data[data.RTs == rt_max].index[0]

    # Max RTs:
    print("The tweet with more retweets is: \n{}".format(data['tweet_textual_content'][id_rt_max]))
    print("Number of retweets: {}".format(rt_max))
    print("{} characters.\n".format(data['len'][id_rt_max]))
    return(data.loc[ id_rt_max])

def tweet_like_max(data):
    like_max  = np.max(data['Likes'])
    id_like_max = data[data.Likes == like_max].index[0]

    # Max RTs:
    print("The tweet with more likes is: \n{}".format(data['tweet_textual_content'][id_like_max]))
    print("Number of retweets: {}".format(like_max))
    print("{} characters.\n".format(data['len'][id_like_max]))
    return(data.loc[ id_like_max])

tweet_like_max(data)
