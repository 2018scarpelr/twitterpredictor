from tweet_collection.to_dataframe import transform_to_dataframe
import numpy as np

def followers_max(data):
    followers_max  = np.max(data['followers'])
    id_followers_max = data[data.followers == followers_max].index[0]

    # Max followers:
    print("The tweet with the user with more followers is: \n{}".format(data['tweet_textual_content'][id_followers_max]))
    print("Number of followers: {}".format(followers_max))
    print("{} characters.\n".format(data['len'][id_followers_max]))
    return(data.loc[ id_followers_max])
