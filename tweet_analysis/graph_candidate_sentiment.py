import seaborn as sns
import pandas as pd
from tweet_analysis.analise_pos_neg_candidate import tweet_sentiment
from tweet_collection.twitter_connection_setup import twitter_setup
from tweet_collection.collect_candidate_actuality_tweets import *
from matplotlib import pyplot as plt

candidate_number = 1
queries = get_candidate_queries(candidate_number, '../CandidateData')
connexion = twitter_setup()
tweets = get_tweets_from_candidates_search_queries(queries, connexion)
porcent_neg,porcent_neu,porcent_pos = tweet_sentiment(tweets)


x=['Negative','Neutre','Positive']
y=[porcent_neg,porcent_neu,porcent_pos]
sns.set(style="whitegrid")

# Load the example Titanic dataset
data=pd.DataFrame(data={'Opinion':x,'Percentage':y})

# Draw a nested barplot to show survival for class and sex
g = sns.catplot(x='Opinion', y='Percentage', data=data,
                height=6, kind="bar", palette="muted",legend=False)
g.fig.suptitle('Tweets opinions of the candidate {}'.format(candidate_number))
plt.show()
