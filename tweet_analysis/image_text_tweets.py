import re
from PIL import Image
from wordcloud import WordCloud
import matplotlib.pyplot as plt
import numpy as np
from textblob import TextBlob
from tweet_collection.to_dataframe import transform_to_dataframe
from tweet_collection.twitter_connection_setup import twitter_setup
from tweet_collection.collect_candidate_actuality_tweets import *

def count_words(tweets):
    data_frame = transform_to_dataframe(tweets)

    all_text = ""
    for text in (data_frame["tweet_textual_content"]):
        all_text+= (text+" ")


    all_text_textblob = TextBlob(all_text)
    all_text_textblob = all_text_textblob.lower()

    clean = set(all_text_textblob.words)
    word_counter = {}

    for word in clean:

        if re.match("a|the|co|an|´|`|the|to|in|for|of|or|by|with|is|on|that|plus|e|se|les|’|qui|http|à|'|par|qu'on|n|ne|rt|our|et|la|le|https|en|une|un|des|de|dans|pour|avec|est|sur|que|je|tu|nous|il|vous|elle|ils|elles|nos|vos|ce|cet|cette|ces|ses|son|sa|du|l|,|sont", word):
            continue
        counter = all_text_textblob.words.count(word)
        word_counter[word] = counter
    return(word_counter)

def makeImage(tweets):
    words_dict = count_words(tweets)

    france_mask = np.array(Image.open("france_mask.png"))

    wc = WordCloud(background_color="white", max_words=1000, mask=france_mask)
    # generate word cloud
    wc.generate_from_frequencies(words_dict)

    # show
    plt.imshow(wc, interpolation="bilinear")
    plt.axis("off")
    plt.show()

if __name__ == '__main__':
    candidate_number = 1
    queries = get_candidate_queries(candidate_number, '../CandidateData')
    connexion = twitter_setup()
    tweets = get_tweets_from_candidates_search_queries(queries, connexion)
    makeImage(tweets)
