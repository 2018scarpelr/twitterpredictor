from textblob import TextBlob
from tweet_collection.twitter_connection_setup import twitter_setup
from tweet_collection.to_dataframe import transform_to_dataframe

def tweet_sentiment(tweets):
    pos_tweets=[]
    neu_tweets=[]
    neg_tweets=[]
    parametre=0.01
    data = transform_to_dataframe(tweets)
    for text_tweet in data['tweet_textual_content']:
        tweet_blob=TextBlob(text_tweet)
        sentim_tweet=tweet_blob.sentiment.polarity
        if sentim_tweet > parametre:
            pos_tweets.append(text_tweet)
        elif sentim_tweet < -parametre:
            neg_tweets.append(text_tweet)
        else:
            neu_tweets.append(text_tweet)

    porcent_pos = len(pos_tweets)*100/len(data['tweet_textual_content'])
    porcent_neg = len(neg_tweets)*100/len(data['tweet_textual_content'])
    porcent_neu = len(neu_tweets)*100/len(data['tweet_textual_content'])
    print("Percentage of positive tweets: {}%".format(porcent_pos))
    print("Percentage of neutral tweets: {}%".format(porcent_neu))
    print("Percentage de negative tweets: {}%".format(porcent_neg))

    return(porcent_neg,porcent_neu,porcent_pos)

if __name__ == "__main__":
    connexion = twitter_setup()
    tweets =connexion.search("Trump", language="english", rpp=1000)
    tweet_sentiment(tweets)
