import pandas as pd
from tweet_collection.twitter_connection_setup import twitter_setup
from tweet_collection.collect_candidate_actuality_tweets import get_candidate_queries, get_tweets_from_candidates_search_queries
import json
from tweet_collection.twitter_search import collect
import numpy as np


def transform_to_dict(tweet):
    return {"tweet_textual_content": str(tweet.text),
            "len" : len(tweet.text),
            "likes": tweet.favorite_count,
            "RTs": tweet.retweet_count,
            "date":tweet.created_at,
            "followers":tweet.user.followers_count}

def transform_to_dataframe(tweets):
    dict_list = []
    for tweet in tweets:
        dict_list.append(transform_to_dict(tweet))
    data_frame = pd.DataFrame(dict_list)
    return data_frame


#######Testing######
if __name__ == "__main__":  # If we execute this file
    connexion = twitter_setup()
    tweets = connexion.search("Emmanuel Macron", language="french", rpp=100)
    data=transform_to_dataframe(tweets)
    print(data.to_string())

    rt_max  = np.max(data['RTs'])
    rt  = data[data.RTs == rt_max].index[0]

    # Max RTs:
    print("The tweet with more retweets is: \n{}".format(data['tweet_textual_content'][rt]))
    print("Number of retweets: {}".format(rt_max))
    print("{} characters.\n".format(data['len'][rt]))
    print(data.loc[rt])

