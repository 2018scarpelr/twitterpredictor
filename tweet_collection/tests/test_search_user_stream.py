from tweet_collection.twitter_connection_setup import twitter_setup
from tweet_collection.twitter_search import collect
from tweet_collection.twitter_streaming import collect_by_streaming
from tweet_collection.twitter_users import collect_by_user

function_to_test = 'user_id' # streaming / user_id

if function_to_test == 'search':
    print('Now testing : twitter_search\n')
    search_terms = ["Emmanuel Macron",
                    "Emmanuel and Macron",
                    "Emmanuel AND Macron",
                    "Emmanuel OR Macron",
                    "Emmanuel -Macron",
                    "#EmmanuelMacron",
                    "@EmmanuelMacron"]
    for search_term in search_terms:
        tweets = collect(search_term)
        for tweet in tweets:
            print('-> {}\n'.format(tweet.text))

elif function_to_test == 'user_id':
    print('Now testing : twitter_users\n')
    # Getting the user ID from the username
    connexion = twitter_setup()
    user_objects = connexion.lookup_users(screen_names=['EmmanuelMacron'])
    user_ids = [user.id_str for user in user_objects]
    # Now get the user stream
    for user_id in user_ids:
        collect_by_user(user_id)


elif function_to_test == 'streaming':
    print('Now testing : twitter_streaming\n')
    search_term = 'Emmanuel Macron'
    collect_by_streaming(search_term)